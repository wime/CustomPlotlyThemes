# Custom Plotly Themes

## Description
The package gives additional themes for plotly. The primary motivation behind the package to have a *solarized light* and  *solarized dark* templates to integrate better with my IDE. 

## Installation
```bash
pip install git+"https://gitlab.com/wime/CustomPlotlyThemes.git"
```

## Usage
Just write the following with the other imports:
```python
from custom_themes import custom_plotly_themes
```
And use it like any other plotly template. The following are available:
* solarized_light \
![Solarized Light](Resources/solarized_light.png)

* solarized_dark \
![Solarized Dark](Resources/solarized_dark.png)
### Own custom themes
To make a template for ther themesthe following can be added:
```python
new_colorscheme = {
    'primary_background': '#002b36',
    'secondary_background': '#073642',
    'primary_foreground': '#839496',
    'secondary_foreground': '#586e75',
    'accent1': '#dc322f',
    'accent2': '#268bd2',
    'accent3': '#859900',
    'accent4': '#d33682',
    'accent5': '#b58900',
    'accent6': '#cb4b16',
    'accent7': '#2aa198',
    'accent8': '#6c71c4',
}
custom_plotly_themes.make_template('new_colorscheme', new_colorscheme)
```
And use `new_colorscheme`like any other template.

## License
The package is published under the MIT license.
